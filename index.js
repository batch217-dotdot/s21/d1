// Section - Arrays

let studentNameA = "2020-1923";
let studentNameB = "2020-1924";
let studentNameC = "2020-1925";
let studentNameD = "2020-1926";

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926"];

// Common example of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

// Possible use of an array but is not recommended
let mixedArr = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way of writing an array

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
]

// Create an array with values from variable
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);

// Section - Length property

let fullName = "Jamie Noble";
console.log(fullName.length);
console.log(cities.length);

// Used .length-1 to delete the last item in an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// Another way to delete an item on an array
cities.length--;
console.log(cities);

// We can't do same on strings
fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

// Adding an item in an array using incrementation (++)
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
theBeatles.length++;
theBeatles.length++;
console.log(theBeatles);

// Section - Reading from arrays

console.log(grades[0]);
console.log(computerBrands[3]);

console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[4]);
console.log(lakersLegends[0]);

// You can also save/store array items in another variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also reassign array values using the items' indices
console.log("Array before re-assignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

// adding items to an array
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barett Wallace";
console.log(newArr);

// Looping over an array
for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40, 123, 32, 1000];

for (let index = 0; index < numArr.length; index++) {
	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5");
	}
	else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Section - Multidimensional array

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.log(chessBoard);

console.log(chessBoard[1][4]);
console.log(chessBoard[4][3]);
console.table(chessBoard);

console.log("Pawn moves to: " + chessBoard[1][5])